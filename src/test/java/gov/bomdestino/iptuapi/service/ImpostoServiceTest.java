package gov.bomdestino.iptuapi.service;

import gov.bomdestino.iptuapi.dto.ImovelDTO;
import gov.bomdestino.iptuapi.model.TipoDocumento;
import gov.bomdestino.iptuapi.repository.PessoaFisicaRepository;
import gov.bomdestino.iptuapi.repository.PessoaJuridicaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

import static gov.bomdestino.iptuapi.stubs.PessoaProvider.buildPessoaFisica;
import static gov.bomdestino.iptuapi.stubs.PessoaProvider.buildPessoaJuridica;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ImpostoServiceTest {
    @Mock
    PessoaFisicaRepository pessoaFisicaRepository;
    @Mock
    PessoaJuridicaRepository pessoaJuridicaRepository;

    ImpostoService impostoService;

    @BeforeEach
    void init(){
        impostoService = new ImpostoService(pessoaFisicaRepository, pessoaJuridicaRepository, new ModelMapper());
    }

    @Test
    void deveriaBuscarOImpostoPorPessoaFisicaSeTipoForCPF(){
        String cpf = "12873548746";
        when(pessoaFisicaRepository.findByCpf(cpf)).thenReturn(Optional.of(buildPessoaFisica()));
        List<ImovelDTO> imoveisPF = impostoService.buscaImoveis(TipoDocumento.CPF, cpf);
        assertEquals(1, imoveisPF.size());
        verify(pessoaFisicaRepository).findByCpf(cpf);
        verifyNoInteractions(pessoaJuridicaRepository);
    }

    @Test
    void deveriaBuscarOImpostoPorPessoaJuridicaSeTipoForCNPJ(){
        String cnpj = "12235465300001";
        when(pessoaJuridicaRepository.findByCnpj(cnpj)).thenReturn(Optional.of(buildPessoaJuridica()));
        List<ImovelDTO> imoveis = impostoService.buscaImoveis(TipoDocumento.CNPJ, cnpj);
        assertEquals(1, imoveis.size());
        verify(pessoaJuridicaRepository).findByCnpj(cnpj);
        verifyNoInteractions(pessoaFisicaRepository);
    }

}