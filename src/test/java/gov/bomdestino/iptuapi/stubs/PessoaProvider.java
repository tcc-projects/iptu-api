package gov.bomdestino.iptuapi.stubs;

import gov.bomdestino.iptuapi.model.*;

import java.math.BigDecimal;
import java.util.List;

public class PessoaProvider {

    public static PessoaFisica buildPessoaFisica(){
        PessoaFisica pf = new PessoaFisica();
        pf.setId(1l);
        pf.setCpf("12873548746");
        pf.setNome("Sérgio Magalhães");
        pf.setImoveis(getImoveis(pf));
        return pf;
    }

    public static PessoaJuridica buildPessoaJuridica(){
        PessoaJuridica pj = new PessoaJuridica();
        pj.setId(1l);
        pj.setCnpj("12873548746");
        pj.setRazaoSocial("Puc-MG");
        pj.setImoveis(getImoveis(pj));
        return pj;
    }

    private static List<Imovel> getImoveis(Proprietario proprietario) {
        Imovel imovel = new Imovel();
        imovel.setEndereco("Rua taubaté");
        imovel.setId(1l);
        imovel.setLat(BigDecimal.ONE);
        imovel.setLng(BigDecimal.ZERO);
        imovel.setProprietario(proprietario);

        Imposto imposto = new Imposto();
        imposto.setTipoImposto(TipoImposto.IPTU);
        imposto.setAnoExercicio(2021);
        imposto.setId(1l);
        imposto.setValor(BigDecimal.TEN);
        imposto.setImovel(imovel);

        imovel.setImpostos(List.of(imposto));

        return List.of(imovel);
    }
}
