package gov.bomdestino.iptuapi.controller;

import gov.bomdestino.iptuapi.model.TipoDocumento;
import gov.bomdestino.iptuapi.service.ImpostoService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ImpostoController.class)
@ExtendWith(SpringExtension.class)
class ImpostoControllerTest {

    @Autowired
    MockMvc http;

    @MockBean
    ImpostoService impostoService;

    @Test
    void deveriaRetornarSucessoSeBuscaComCPFEstiverCorretamentePreenchida() throws Exception {
        when(impostoService.buscaImoveis(TipoDocumento.CPF, "12873548746"))
            .thenReturn(new ArrayList<>());
        http.perform(get("/imoveis")
                        .queryParam("documento", "12873548746")
                        .queryParam("tipoDocumento", "CPF"))
                .andExpect(status().isOk());
    }
    @Test
    void deveriaRetornarSucessoSeBuscaComCNPJEstiverCorretamentePreenchida() throws Exception {
        when(impostoService.buscaImoveis(TipoDocumento.CPF, "12873548746"))
                .thenReturn(new ArrayList<>());
        http.perform(get("/imoveis")
                .queryParam("documento", "3216598734654645")
                .queryParam("tipoDocumento", "CNPJ"))
                .andExpect(status().isOk());
    }

    @Test
    void deveriaDarBadRequestSeTipoDocumentoNaoForReconhecido() throws Exception {
        http.perform(get("/imoveis")
                .queryParam("documento", "12873548746")
                .queryParam("tipoDocumento", "OPEN"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deveriaDarBadRequestSeFaltarTipoDocumento() throws Exception {
        http.perform(get("/imoveis")
                .queryParam("documento", "12873548746"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deveriaDarBadRequestSeFaltarDocumento() throws Exception {
        http.perform(get("/imoveis")
                .queryParam("tipoDocumento", "CPF"))
                .andExpect(status().isBadRequest());
    }


    @Test
    void deveriaDarBadRequestSeDocumentoNaoForApenasNumeros() throws Exception {
        http.perform(get("/imoveis")
                        .queryParam("documento", "AB1234564555")
                        .queryParam("tipoDocumento", "CNPJ"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.erros").value(hasSize(1)))
                .andExpect(jsonPath("$.erros[0].message").value("O numero do documento deve ter apenas digitios"));
    }
}