package gov.bomdestino.iptuapi.model;

public enum TipoImposto {
    ITR,
    IPTU
}
