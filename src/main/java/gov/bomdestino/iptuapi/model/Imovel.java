package gov.bomdestino.iptuapi.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Entity
public class Imovel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(precision = 19, scale = 6, columnDefinition="DECIMAL(19,6)")
    private BigDecimal lat;
    @Column(precision = 19, scale = 6, columnDefinition="DECIMAL(19,6)")
    private BigDecimal lng;
    private String endereco;
    private String inscricao;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Imposto> impostos;
    @Any(metaColumn = @Column(name = "tipo_proprietario"))
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = {
            @MetaValue(value = "PF", targetEntity = PessoaFisica.class),
            @MetaValue(value = "PJ", targetEntity = PessoaJuridica.class) })
    @JoinColumn(name = "proprietario_id")
    private Proprietario proprietario;
}
