package gov.bomdestino.iptuapi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
public class Imposto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer anoExercicio;
    private BigDecimal valor;
    @ManyToOne
    private Imovel imovel;
    @Enumerated(EnumType.STRING)
    private TipoImposto tipoImposto;
}
