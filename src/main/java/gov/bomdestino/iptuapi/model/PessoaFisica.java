package gov.bomdestino.iptuapi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class PessoaFisica implements Proprietario{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String cpf;
    @OneToMany
    private List<Imovel> imoveis;

    @Override
    public String getDocumento() {
        return cpf;
    }
}
