package gov.bomdestino.iptuapi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class PessoaJuridica implements Proprietario{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String razaoSocial;
    private String cnpj;
    @OneToMany
    private List<Imovel> imoveis;

    @Override
    public String getDocumento() {
        return cnpj;
    }

    @Override
    public String getNome() {
        return razaoSocial;
    }
}
