package gov.bomdestino.iptuapi.model;

import java.util.List;

public interface Proprietario {
    String getDocumento();
    String getNome();
    List<Imovel> getImoveis();
}
