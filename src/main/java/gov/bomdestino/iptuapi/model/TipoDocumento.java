package gov.bomdestino.iptuapi.model;

public enum TipoDocumento {
    CPF,
    CNPJ
}
