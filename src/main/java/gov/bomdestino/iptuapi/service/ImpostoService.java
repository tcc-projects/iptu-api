package gov.bomdestino.iptuapi.service;

import gov.bomdestino.iptuapi.dto.ImovelDTO;
import gov.bomdestino.iptuapi.model.*;
import gov.bomdestino.iptuapi.repository.PessoaFisicaRepository;
import gov.bomdestino.iptuapi.repository.PessoaJuridicaRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ImpostoService {
    private final PessoaFisicaRepository pessoaFisicaRepository;
    private final PessoaJuridicaRepository pessoaJuridicaRepository;
    private final ModelMapper modelMapper;

    public List<ImovelDTO> buscaImoveis(TipoDocumento tipoDocumento, String numeroDocumento){
        Proprietario proprietario = null;
        if(TipoDocumento.CPF ==tipoDocumento){
            proprietario= pessoaFisicaRepository.findByCpf(numeroDocumento)
                                               .orElseThrow(EntityNotFoundException::new);
            return buildImoveisDTO(proprietario);
        }else if(TipoDocumento.CNPJ == tipoDocumento){
            proprietario = pessoaJuridicaRepository.findByCnpj(numeroDocumento)
                                                    .orElseThrow(EntityNotFoundException::new);
            return buildImoveisDTO(proprietario);
        }
        return new ArrayList<>();
    }

    private List<ImovelDTO> buildImoveisDTO(Proprietario proprietario){
        return proprietario.getImoveis()
                            .stream()
                            .map(imovel -> modelMapper.map(imovel, ImovelDTO.class))
                            .map(d-> {d.setNomeProprietario(proprietario.getNome());
                                     return d;})
                            .collect(Collectors.toList());
    }
}
