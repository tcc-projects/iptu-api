package gov.bomdestino.iptuapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class ImovelDTO {
    private String endereco;
    private Localizacao localizacao;
    private String inscricao;
    private String nomeProprietario;
    private List<ImpostoDTO> impostos;

    public void setLat(BigDecimal lat){
        getLocalizacaoInstance().lat = lat;
    }

    public void setLng(BigDecimal lng){
        getLocalizacaoInstance().lng = lng;
    }

    private Localizacao getLocalizacaoInstance(){
        if(this.localizacao==null){
            this.localizacao = new Localizacao();
        }
        return this.localizacao;
    }
}
@Getter
class Localizacao {
    BigDecimal lat;
    BigDecimal lng;
}
