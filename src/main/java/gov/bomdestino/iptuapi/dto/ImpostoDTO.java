package gov.bomdestino.iptuapi.dto;

import gov.bomdestino.iptuapi.model.TipoImposto;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
@Getter
@Setter
public class ImpostoDTO {
    private Integer exercicio;
    private BigDecimal valor;
    private TipoImposto tipo;
}
