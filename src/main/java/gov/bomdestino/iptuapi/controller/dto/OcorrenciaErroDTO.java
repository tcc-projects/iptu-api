package gov.bomdestino.iptuapi.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OcorrenciaErroDTO {

	private String referencia;
	private String message;

	public OcorrenciaErroDTO(String message, String referencia) {
		this.message = message;
		this.referencia = referencia;
	}

}
