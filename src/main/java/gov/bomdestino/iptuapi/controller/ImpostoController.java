package gov.bomdestino.iptuapi.controller;


import gov.bomdestino.iptuapi.dto.ImovelDTO;
import gov.bomdestino.iptuapi.model.TipoDocumento;
import gov.bomdestino.iptuapi.service.ImpostoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;

import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;
import java.util.List;

@RequestMapping(value = "/imoveis", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
@Validated
public class ImpostoController {
    private final ImpostoService impostoService;

    @GetMapping
    @ApiOperation(value = "Obtém todos os IPTUs/ITRs de um proprietário")
    public List<ImovelDTO> buscaImoveisPorDocumento(@ApiParam(value = "Documento do proprietário do imóvel", required = true)
                                                    @RequestParam("documento")
                                                    @Pattern(regexp = "^[0-9]*$", message="O numero do documento deve ter apenas digitios")
                                                                String numeroDocumento,
                                                    @ApiParam(value = "Tipo do Documento do proprietário", required = true,allowableValues ="CPF,CNPJ")
                                                    @RequestParam("tipoDocumento")
                                                            TipoDocumento tipoDocumento){
        return impostoService.buscaImoveis(tipoDocumento, numeroDocumento);
    }
}
