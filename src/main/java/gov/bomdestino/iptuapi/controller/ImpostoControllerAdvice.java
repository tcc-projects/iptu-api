package gov.bomdestino.iptuapi.controller;


import gov.bomdestino.iptuapi.controller.dto.ErroDTO;
import gov.bomdestino.iptuapi.controller.dto.OcorrenciaErroDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;

@ControllerAdvice
public class ImpostoControllerAdvice {
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErroDTO> methodArgumentReponse(ConstraintViolationException ex) {
        ErroDTO erroDTO = new ErroDTO();
        erroDTO.setDataErro(LocalDateTime.now());
        erroDTO.setHttpCode(400);

        ex.getConstraintViolations().forEach(violation -> {
            erroDTO.addOcorrencia(new OcorrenciaErroDTO(violation.getMessage(), violation.getPropertyPath().toString()));
        });
        return ResponseEntity.badRequest().body(erroDTO);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity methodArgumentReponse(EntityNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

}
