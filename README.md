## Descrição:
Este software prove uma interface REST integração com o sistema legado de consulta de impostos da prefeitura de bom destino


## Tecnologias
Neste projeto foi codificado em **Java** utilizando set de frameworks **Spring** :
* Boot
* Data
* MVC

## Banco de dados
MySQL
